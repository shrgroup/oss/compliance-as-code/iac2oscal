from pyoscal.core.OSCAL import OSCAL
from pyoscal.SSP import SSP
from pyoscal.ComponentDef import ComponentDefinition
import yaml, sys, os

script_dir = os.path.dirname(os.path.realpath(__file__))

with open(sys.argv[1]) as f:
    input_data = yaml.safe_load(f)

ssp = SSP(
    title="System Security Plan (SSP) for {}".format(input_data.get('title'))
)
ssp.set_info(
    name=input_data['title'],
    description=input_data['description']
)
ssp.set_cia(
    c=input_data['level']['c'],
    i=input_data['level']['i'],
    a=input_data['level']['a']
)
for user in input_data.get('users',[]):
    ssp.add_user(
        title=user.get('name'),
        role_id=user.get('role')
    )

for person in input_data.get('people', []):
    role = person
    person = input_data.get('people', [])[person]
    ssp.add_party(
        role=role,
        name=person.get('name'),
        title=person.get('title','N/A'),
        org=person.get('organization','N/A'),
        address=person.get('address','N/A'),
        phone=person.get('phone','N/A'),
        email=person.get('email', 'N/A')
    )

cdefs = []
for path in input_data['components']:
    path = os.path.join(script_dir, path)
    cdef = ComponentDefinition.from_file(path)
    for control in cdef.get_controls():
        ssp.add_control(
            component_id=cdef.uuid,
            control_id=control['control_id'],
            narrative=control['narrative'].strip(),
        )
    ssp.add_component(
        title=cdef.title,
        description=cdef.description,
        type=cdef.component_type
    )

with open(sys.argv[2], 'w') as f:
    f.write(ssp.export())

