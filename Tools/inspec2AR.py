from pyoscal.core.OSCAL import OSCAL
from pyoscal.AR import AR
import yaml 

ar = AR(
    title="Automated Results"
)

inspec_result_path = '../iac/generated/output.yaml'
with open(inspec_result_path) as f:
    results = yaml.safe_load(f)
controls = results[':profiles'][0][':controls']
for control in controls:
    ar.add_result(
        title=control.get(':title','NOTITLE'),
        description=control.get(':desc', ""),
        start=control[':results'][0][':start_time'],
        controls=[control[':id']],
        finding=(
            control[':results'][0][':status'],
            control[':results'][0][':code_desc']
        )
    )

xml = ar.export()
with open('../iac/generated/ar.xml') as f:
    f.write(xml)
