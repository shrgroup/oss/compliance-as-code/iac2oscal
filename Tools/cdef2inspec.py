from pyoscal.core.OSCAL import OSCAL
from pyoscal.ComponentDef import ComponentDefinition, Component, Capability
import base64, sys, os
from jinja2 import Template
from subprocess import check_output
import yaml

script_dir = os.path.dirname(os.path.realpath(__file__))

oscal = OSCAL()

inputFile = sys.argv[1]
oscal = ComponentDefinition.from_file(inputFile)

validation = []
for control in oscal.get_controls():
    obj = {
        'control_id': control.get('control_id'),
        'narrative': control.get('narrative'), 
    }
    for prop in control.get('props',[]):
        if prop[0] == 'validation':
            vtype = prop[1]
            decoded = base64.b64decode(prop[2]).decode("utf-8")
            if vtype not in obj:
                obj[vtype] = ""
            obj[vtype] += decoded
    validation += [obj]
    
templatepath = os.path.join(script_dir,'templates/inspec_profile.j2')
with open(templatepath) as t:
    template = Template(t.read())
inspec_script = template.render(
    cdef_title=oscal.title,
    controls=validation
    )
inspec_path='iac/generated/inspec_script.rb'
with open(inspec_path, 'w') as f:
    f.write(inspec_script)

config_path='iac/generated/instances.yaml'
with open(config_path, 'r') as f:
    instances = yaml.safe_load(f)

cmds = []
output_path='iac/generated/inspec_results.yaml'
key_file = 'iac/generated/{}.pem'.format(oscal.uuid)
ip = instances.get(oscal.uuid)
cmds += ["/opt/inspec/bin/inspec --chef-license=accept"]
cmds += ["/opt/inspec/bin/inspec exec {2} --key-files {0} --target ssh://ubuntu@{1} --reporter yaml:{3}".format(
        key_file,
        ip,
        inspec_path,
        output_path
    )]

inspec_script_path = 'iac/generated/inspec_runme.sh'
with open(inspec_script_path, 'w') as f:
    f.writelines(cmds)
