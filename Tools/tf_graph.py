from subprocess import check_output
import re

keep = []
output = ['digraph {'.encode()]
digraph = check_output(['/bin/terraform','graph'])
resource_pattern = re.compile("\t\t(\".*\").\[label.*")
relation_pattern = re.compile("\t\t(\".*\").->.(\".*\")")
for line in digraph.split(b'\n'):
    if b'box' in line:
        object_match = resource_pattern.search(line.decode())
        if not object_match:
            print(":( {}".format(line))
        else:
            # print(":) {}".format(object_match.group(1)))
            keep += [object_match.group(1)]
        output += [line]
    if b'->' in line:
        object_match = relation_pattern.search(line.decode())
        left = object_match.group(1)
        right = object_match.group(2)
        if left in keep and right in keep:
            output += [line]
output += ['}'.encode()]
with open('cleaner_diagram.dot','w') as f:
    f.write(b'\n'.join(output).decode())