# Consolodated

All the phases in one repo for initial poc

## Things not checked in

file in `iac/aws.pkrvars.hcl` 

```
aws_access_key = "<<REDACTED>>"
aws_secret_key = "<<REDACTED>>"
aws_build_vpc = "vpc-<<REDACTED>>"
aws_build_subnet = "subnet-<<REDACTED>>"
```
## Testing 

Build the AMI and Associated CDEF 

`packer build -var-file="aws.pkrvars.hcl" ubuntu.pkr.hcl`
`docker run -v ${PWD}/iac:/data -it --entrypoint=packer hashiansible  build -var-file="aws.pkrvars.hcl" ubuntu.pkr.hcl`

Build the CDEF from Terraform 
`ansible-playbook ansible/TF_CDEF.yml`

Deploy the Test Machine
`terraform init # For first time`
`terraform apply -auto-approve -var-file="ami.tfvars" -var-file="aws.pkrvars.hcl"`

Full command from testing 
`docker run -v ${PWD}/iac:/data -it --entrypoint=/bin/terraform hashiansible apply -auto-approve -var-file="ami.tfvars" -var-file="aws.pkrvars.hcl"`

Build SSP
TBD

Run Tests and Build AR
Prep tests
`docker run -v ${PWD}:/data -it --entrypoint=python hashiansible Tools/cdef2inspec.py iac/generated/ami_cdef.xml`
Execute Tests
`docker run -v ${PWD}:/share -it --entrypoint sh chef/inspec /share/iac/generated/inspec_runme.sh`

## Diagram 

```plantuml
@startuml

left to right direction 

card "Source\nAMI" as source_ami 
rectangle "Packer" {
    component "Packer" as packer 
    component "Ansible" as ansible
}

file "CDEF" as ami_cdef {
    card "Packages" as ami_cdef_packages
    card "Controls" as ami_cdef_controls
    card "Verification" as ami_cdef_verify
    ami_cdef_controls --> ami_cdef_verify
}
card "Built\nAMI" as ami

source_ami --> packer 
packer <--> ansible 
packer --> ami 
ansible --> ami_cdef_packages 
ansible --> ami_cdef_controls

rectangle "Terraform" {
    component "Terraform" as terraform 
    component "TF Parser" as tf_parser
}
file "CDEF" as tf_cdef {
    card "Capabilities" as tf_cdef_capabilities
    card "Controls" as tf_cdef_controls
    card "Narrative" as tf_cdef_narrative
    card "Verification" as tf_cdef_verify
    card "Components" as tf_cdef_components
    tf_cdef_controls --> tf_cdef_narrative
    tf_cdef_controls --> tf_cdef_verify

}

terraform --> tf_parser 
tf_parser --> tf_cdef_controls
tf_parser --> tf_cdef_capabilities
tf_parser --> tf_cdef_components
' ami_cdef -> tf_parser

ami --> terraform

rectangle "Doc Builder" as doc_builder {
    component "System Inventory" as sys_inventory
    component "Inspec List" as consolodate_inspec 
    component "OSCAL Build" as oscal_build  
    consolodate_inspec -> oscal_build
}
file "System Info" as sys_info 

sys_info --> oscal_build
tf_cdef_narrative --> oscal_build


ami_cdef --> sys_inventory
tf_cdef_components --> sys_inventory

ami_cdef_verify --> consolodate_inspec
tf_cdef_verify --> consolodate_inspec


file "Inspec Checklist" as inspec_profile
file "SBOM" as sbom 
file "SSP" as ssp 
consolodate_inspec --> inspec_profile
sys_inventory --> sbom 
oscal_build --> ssp 

file "Assessment\nPlan" as ap 
oscal_build --> ap 

file "Assessment\nResults" as ar
file "PoaM" as poam 
cloud "Live System" as live_system 
rectangle "Assessment" {
    component "Inspec" as inspec 
    component "Parser" as assessment_parser
    file "inspec results" as inspec_results
    inspec --> inspec_results
    inspec_results --> assessment_parser
}
ssp --> assessment_parser
inspec_profile --> inspec 
inspec <-> live_system 
terraform --> live_system
assessment_parser --> ar 
assessment_parser --> poam

@enduml
```