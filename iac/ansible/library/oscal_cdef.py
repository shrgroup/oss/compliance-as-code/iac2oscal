#!/usr/bin/python

import base64

from ansible.module_utils.basic import *
from pyoscal.OSCAL import OSCAL
from pyoscal.ComponentDef import ComponentDefinition, Component, Capability

def create_cdef(title='no title', type=None):
    data=[('type',type)]
    cdef = ComponentDefinition(
        title="Test Component Defintion",
        description="Description of Test Component",
        data=data
    )
    return cdef
def add_controls(cdef,controls):
    capability = Capability(
        name='AMI',
        description=''
    )
    for control in controls:
        validation=str(base64.b64encode(control['validation'].encode('utf-8')))
        validation = validation.replace("b'","").replace("'","")
        capability.add_control(
            controlid=control['id'],
            narrative=control['narrative'],
            props=[('validation',control.get('validation_type','inspec'), validation)]
        )
    cdef.add_capability(capability)

def add_package(cdef, package_name, versions):
    versions = [p['version'] for p in versions]
    component = Component(
        title=package_name,
        type='package',
        description="versions: {}".format(', '.join(versions))
    )
    cdef.add_component(component)

def add_pps(cdef, services):
    cdef.add_pps(services)

def add_resource(cdef, resource):
    component = Component(
        title=resource['name'],
        type=resource['type']
    )
    cdef.add_component(component)

def get_xml(cdef):
    xml = cdef.export()
    return xml 

def main():
    module = AnsibleModule(
        argument_spec=dict(
            title=dict(type='str', required=True),
            type=dict(type='str', default='Image'),
            packages=dict(type='dict', default={}),
            resources=dict(type='dict', default={}),
            controls=dict(type='list', default=[]),
            services=dict(type='dict', default=[]),
            destination=dict(type='str', required=False, default='cdef.xml')
        )
    )
    result = dict(
        changed=False,
        output='',
        uuid=''
    )

    cdef = create_cdef(module.params['title'])
    result['uuid'] = cdef.oscal.uuid.prose

    packages = module.params['packages']
    for package in packages:
        add_package(cdef, package, packages[package])

    resources = module.params['resources'].get('managed_resources',{})
    for resource in resources:
        add_resource(cdef, resources[resource])

    add_controls(cdef,module.params['controls'])
    add_pps(cdef,module.params['services'])

    result['output'] = get_xml(cdef)

    module.exit_json(**result)
 
if __name__ == "__main__":
    main()