# copyright: 2016, Chef Software, Inc.

title 'Test Component Defintion'


control 'etc-1' do
  impact 'critical'
  title 'All systems have a /etc directory'

  describe directory('/etc') do
    it { should exist }
  end

end
