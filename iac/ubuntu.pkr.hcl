
variable "aws_access_key" {
  type    = string
  default = ""
}

variable "aws_build_subnet" {
  type    = string
  default = ""
}

variable "aws_build_vpc" {
  type    = string
  default = ""
}

variable "aws_secret_key" {
  type    = string
  default = ""
}

data "amazon-ami" "ubuntu_ami" {
  access_key = "${var.aws_access_key}"
  filters = {
    name                = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
    root-device-type    = "ebs"
    virtualization-type = "hvm"
  }
  most_recent = true
  owners      = ["099720109477"]
  region      = "us-east-1"
  secret_key  = "${var.aws_secret_key}"
}

source "amazon-ebs" "rancher_server" {
  access_key                  = "${var.aws_access_key}"
  ami_name                    = "Rancher Server"
  force_deregister            = true
  force_delete_snapshot       = true
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  region                      = "us-east-1"
  secret_key                  = "${var.aws_secret_key}"
  source_ami                  = "${data.amazon-ami.ubuntu_ami.id}"
  ssh_username                = "ubuntu"
  subnet_id                   = "${var.aws_build_subnet}"
  vpc_id                      = "${var.aws_build_vpc}"
}

build {
  sources = ["source.amazon-ebs.rancher_server"]
  provisioner "ansible" {
      user             = "ubuntu"
      playbook_file    = "./ansible/CDEF.yml"
      extra_arguments  = [ "--extra-vars", "@ami_controls.yml" ]
    }
  post-processor "manifest" {
        output = "manifest.json"
        strip_path = true
    }
  post-processor "shell-local" {
    inline = [
      "sed -i \"s/##AMIID##/$(jq '.builds[-1].artifact_id' manifest.json)/g\" ami.tfvars",
      "sed -i \"s/us-east-1://g\" ami.tfvars",
      "rm manifest.json"
      ]
  }

}
