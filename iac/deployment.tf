terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

variable "ami_id" {
  type        = string
  description = "AMI ID for deployed instances"
}
variable "cdef_uuid" {
  type        = string
  description = "OSCAL UUID of CDEF"
}
variable "aws_access_key" {
  type        = string
  description = "AWS access key used to create infrastructure"
}
variable "aws_secret_key" {
  type        = string
  description = "AWS secret key used to create AWS infrastructure"
}
variable "aws_build_vpc" {
  type        = string
  description = "VPC to put the Instance"
}

provider "aws" {
  profile = "default"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region  = "us-east-1"
}

resource "tls_private_key" "sshkey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.cdef_uuid
  public_key = tls_private_key.sshkey.public_key_openssh
}

resource "aws_instance" "OSCAL_Simple_Server" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.generated_key.key_name

  tags = {
    OSCALUUID = var.cdef_uuid
  }
}

resource "local_file" "private_key" {
  content         = tls_private_key.sshkey.private_key_pem
  filename        = "generated/${var.cdef_uuid}.pem"
  file_permission = "0600"
}
resource "local_file" "instances" {
  content         = "${var.cdef_uuid}: ${resource.aws_instance.OSCAL_Simple_Server.public_ip}"
  filename        = "generated/instances.yaml"
  file_permission = "0600"
}