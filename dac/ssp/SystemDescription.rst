General System Description
==========================

Imported Components
-------------------

.. jinja:: builtins

   {% set ssp=builtins['ssp'] %}
   {% for component in ssp.components %}
   {{component.title}}
   {{ '^'*component.title|length }}
   
   Type: {{component.type}}

   {{component.description}}
   
   {% endfor %}

Users
-----

.. note:: todo
    Create User and Roles list 

Diagram
-------

.. note:: todo
