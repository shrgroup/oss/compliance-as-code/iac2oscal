Assignment Responsibility
=========================

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% if ssp.ISSO %}
    .. list-table:: ISSO Point of Contact

        * - Name
          - {{ssp.ISSO.name}}
        * - Title
          - {{ssp.ISSO.title}}
        * - Company/Organization
          - {{ssp.ISSO.organization}}
        * - Address 
          - {{ssp.ISSO.address | replace('\n',', ')}}
        * - Phone Number 
          - {{ssp.ISSO.phone}}
        * - Email Address 
          - {{ssp.ISSO.email}}
    {% else %}
    No ISSO Specified
    {% endif %}

    {% if ssp.AO %}
    .. list-table:: AO Point of Contact

        * - Name
          - {{ssp.AO.name}}
        * - Title
          - {{ssp.AO.title}}
        * - Company/Organization
          - {{ssp.AO.organization}}
        * - Address 
          - {{ssp.AO.address | replace('\n',', ')}}
        * - Phone Number 
          - {{ssp.AO.phone}}
        * - Email Address 
          - {{ssp.AO.email}}
    {% else %}
    No AO Specified
    {% endif %}