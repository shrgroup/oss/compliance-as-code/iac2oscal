System Name
===========

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    This System Security Plan provides an overview of the security requirements for {{ssp.name}} and describes the controls in place or planned for implementation to provide a level of security appropriate for the information to be transmitted, processed or stored by the system.  Information security is vital to our critical infrastructure and its effective performance and protection is a key component of our national security program.  Proper management of information technology systems is essential to ensure the confidentiality, integrity and availability of the data transmitted, processed or stored by the Enter Information System Abbreviation information system.  

    The security safeguards implemented for the Enter Information System Abbreviation system meet the policy and control requirements set forth in this System Security Plan.  All systems are subject to monitoring consistent with applicable laws, regulations, agency policies, procedures and practices.  

    .. list-table:: Information System Name and Title
        :widths: 40 40 20
        :header-rows: 1

        * - Unique Identifier
          - Information System Name
          - Information System Abbreviation
        * - {{ssp.system_id}}
          - {{ssp.name}}
          - N/A