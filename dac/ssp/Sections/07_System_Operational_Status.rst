System Operational Status
=========================

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}

    .. list-table:: System Status
       :header-rows: 1

       * - Component
         - Status
    {%- for c in ssp.components %}
       * - {{c.title}}
         - {{c.status}}
    {% endfor %}