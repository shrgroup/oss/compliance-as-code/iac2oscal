System Description
==================

System Function
---------------

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {{ssp.description}}

Information System Components and Boundaries
--------------------------------------------

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {{ssp.authorization_boundary}}

Types of Users
--------------

All personnel have their status categorized with a sensitivity level in accordance with PS-2.  Personnel (employees or contractors) of service providers are considered Internal Users.  All other users are considered External Users.  User privileges (authorization permission after authentication takes place) are described in Table 9 1. Personnel Roles and Privileges that follows.

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% if ssp.users %}
    .. list-table:: System Users
       :header-rows: 1

       * - Role
         - Internal or External
         - Privileged or Non-Privileged
         - Sensitivity Level 
         - Authorized Privileges 
         - Functions Performed
    {%- for user in ssp.users %}
       * - {{u.role}}
         - {{u.privileged}}
         - {{u.sensitivity}}
         - {{u.privileges | join(', ')}}
         - {{u.functions}}
    {% endfor %}
    {% else %}
    No Users Defined
    {% endif %}

Network Architecture
--------------------

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% set arch=ssp.network_architecture %}
    {{arch.description}}

    {% if archdiagram %}
    .. uml::
        :caption: {{arch.caption}}

        {{arch.diagram}}
    {% endif %}