Other Contacts
==============

The following individual(s) identified below possess in-depth knowledge of this system and/or its functions and operation.  

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% for p in ssp.parties %}
    .. list-table:: {{p.name}}

        * - Name
          - {{p.name}}
        * - Title
          - {{p.title}}
        * - Company/Organization
          - {{p.organization}}
        * - Address 
          - {{p.address | replace('\n',', ')}}
        * - Phone Number 
          - {{p.phone}}
        * - Email Address 
          - {{p.email}}
    {% endfor %}