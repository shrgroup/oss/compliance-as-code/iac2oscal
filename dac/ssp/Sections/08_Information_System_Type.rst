Information System Type
=======================

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}

    {% for it in ssp.information_types %}
    {{it.type}}
    {{"-"*it.type|length}}

    {{it.description}}

    {% endfor %}