Security Controls
=================

Minimum Security Controls 

Implemented Controls 
--------------------

.. jinja:: builtins

   {% set ssp=builtins['ssp'] %}
   {% for control in ssp.controls %}
   {{control}}
   {{ '-'*control|length }}
   {% set cont = ssp.controls[control] %}
   {% for comp in cont %}
   {{comp.uuid}}
   {{ '^'*comp.uuid|length }}
   {{comp.narrative}}
   {% endfor %}
   {% endfor %}


All Controls
------------

.. jinja:: builtins

   {% set catalogs=builtins['catalogs'] %}
   {% set ssp=builtins['ssp'] %}
   {% for catalog in catalogs %}
   {% for group in catalog.controls %}
   {{group}}
   {{ '^'*group|length }}
   {% for control in catalog.controls[group] |sort(attribute='id') %}
   {{control.id}}: {{control.title}}
   {{ '"'*( ( (control.id|string + control.title)|length ) + 2 ) }}

   {% if group.guidance %}
   **Guidance**: {{group.guidance}}
   {%endif%}

   {% if group.statement %}
   **Statement**: {{group.statement}}
   {%endif%}

   {% for child in control.children |sort(attribute='id') %}
   {{child.id}}: {{child.title}}
   {{ '"'*( ( (child.id|string + child.title)|length ) + 2 ) }}

   {% if child.guidance %}
   **Guidance**: {{child.guidance}}
   {%endif%}
   
   {% if child.statement %}
   **Statement**: {{child.statement}}
   {%endif%}

   .. list-table:: Control Summary for {{child.id}}
      :header-rows: 1

      * - {{child.id}}
        - Control Summary Information
      * - Responsible Role 
        - <INSERT ROLE>
      {%- for param in child.parameters %}
      * - Parameter {{param.id}}:
        - {{param.name}}
      {% endfor %}
      * - Implementation Status
        - {% if child.id in ssp.controls %}Implemented{%else%}Not Applicable{%endif%}
      * - Control Origination
        - TODO

   
   {% endfor %}{# For child in control #}
   {% endfor %}{# For control in group #}
   {% endfor %}{# For group in catalog #}
   {% endfor %}{# For catalog #}