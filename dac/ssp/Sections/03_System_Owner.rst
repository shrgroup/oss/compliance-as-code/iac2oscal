System Owner
============

The following individual is identified as the system owner or functional proponent/advocate for this system.  

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% if ssp.owner %}
    .. list-table:: Information System Owner
       :name: Information System Owner

       * - Name
         - {{ssp.owner.name}}
       * - Title
         - {{ssp.owner.title}}
       * - Company/Organization
         - {{ssp.owner.organization}}
       * - Address 
         - {{ssp.owner.address | replace('\n',', ')}}
       * - Phone Number 
         - {{ssp.owner.phone}}
       * - Email Address 
         - {{ssp.owner.email}}
    {% else %}
    No Owner Specified
    {% endif %}