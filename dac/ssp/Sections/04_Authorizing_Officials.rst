Authorizing Officials
=====================

The Authorizing Official (AO) or Designated Approving Authority (DAA) for this information system is the Insert AO information as instructed above.

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% if ssp.AO %}
    .. list-table:: Information System AO

        * - Name
          - {{ssp.AO.name}}
        * - Title
          - {{ssp.AO.title}}
        * - Company/Organization
          - {{ssp.AO.organization}}
        * - Address 
          - {{ssp.AO.address | replace('\n',', ')}}
        * - Phone Number 
          - {{ssp.AO.phone}}
        * - Email Address 
          - {{ssp.AO.email}}
    {% else %}
    No AO Specified
    {% endif %}