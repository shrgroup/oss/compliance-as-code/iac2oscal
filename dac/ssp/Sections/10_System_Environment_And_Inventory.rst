System Environment And Inventory
================================

Data Flow 
---------

The data flow in and out of the system boundaries is represented in Figure 10-1. Data Flow Diagram, below.

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% set arch=ssp.data_flow %}
    {{arch.description}}

    {% if archdiagram %}
    .. uml::
        :caption: {{arch.caption}}

        {{arch.diagram}}
    {% endif %}

Ports, Protocols, and Services
------------------------------

The Table 10 1. Ports, Protocols and Services below lists the ports, protocols and services enabled in this information system.  

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% if ssp.pps %}
    .. list-table:: PPS
       :header-rows: 1

       * - Ports
         - Protocols
         - Services
         - Purpose
         - Used By 
    {%- for p in ssp.pps %}
        * - {{p.ports}}
          - {{p.protocols}}
          - {{p.services}}
          - {{p.purpose}}
          - {{p.component}}
    {% endfor %}
    {% else %}
    NO PPS Defined
    {% endif %}




                    