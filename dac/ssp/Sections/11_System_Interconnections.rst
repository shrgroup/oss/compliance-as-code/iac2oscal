System Interconnections
=======================

Table 11 1 System Interconnections below is consistent with Table 13 3 CA-3 Authorized Connections.

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% if ssp.interconnections %}
    .. list-table:: Interconnections
       :header-rows: 1

       * - SP* IP Address and Interface
         - External Organization Name
         - External Point of Contact and Phone Number
         - Connection Security
         - Data Direction
         - Information Being Transmitted
         - Port or Circuit Numbers
    {%- for ic in ssp.interconnections %}
       * - {{ic.local_address}}
         - {{ic.org}}
         - {{ic.poc}}
         - {{ic.security}}
         - {{ic.direction}}
         - {{ic.information}}
         - {{ic.portnum}}
    {% endfor %}
    {% else %}
    No Interconnections Defined
    {% endif %}
