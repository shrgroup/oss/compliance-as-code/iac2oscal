System Categorization
=====================

The overall information system sensitivity categorization is recorded in :numref:`System Sensitivity Level` Security Categorization that follows.  

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% set cia=ssp.cia %}
    .. list-table:: Security Categorization
        :name: System Sensitivity Level

        * - System Sensitivity Level
        * - {{ssp.sensitivity}}

Information Types
-----------------

This section describes how the information types used by the information system are categorized for confidentiality, integrity and availability sensitivity levels.  
The following tables identify the information types that are input, stored, processed and/or output from Enter Information System Abbreviation.  The selection of the information types is based on guidance provided by Office of Management and Budget (OMB) Federal Enterprise Architecture Program Management Office Business Reference Model 2.0 and FIPS Pub 199, Standards for Security Categorization of Federal Information and Information Systems which is based on NIST Special Publication (SP) 800-60, Guide for Mapping Types of Information and Information Systems to Security Categories.  

The tables also identify the security impact levels for confidentiality, integrity and availability for each of the information types expressed as low, moderate, or high.  The security impact levels are based on the potential impact definitions for each of the security objectives (i.e., confidentiality, integrity and availability) discussed in NIST SP 800-60 and FIPS Pub 199.  

The potential impact is low if

- The loss of confidentiality, integrity, or availability could be expected to have a limited adverse effect on organizational operations, organizational assets, or individuals.  
- A limited adverse effect means that, for example, the loss of confidentiality, integrity, or availability might: 
    - (i) cause a degradation in mission capability to an extent and duration that the organization is able to perform its primary functions, but the effectiveness of the functions is noticeably reduced; 
    - (ii) result in minor damage to organizational assets; 
    - (iii) result in minor financial loss; or 
    - (iv) result in minor harm to individuals. 

The potential impact is moderate if

- The loss of confidentiality, integrity, or availability could be expected to have a serious adverse effect on organizational operations, organizational assets, or individuals.  
- A serious adverse effect means that, for example, the loss of confidentiality, integrity, or availability might: 
    - (i) cause a significant degradation in mission capability to an extent and duration that the organization is able to perform its primary functions, but the effectiveness of the functions is significantly reduced; 
    - (ii) result in significant damage to organizational assets; 
    - (iii) result in significant financial loss; or 
    - (iv) result in significant harm to individuals that does not involve loss of life or serious life threatening injuries.

The potential impact is high if

- The loss of confidentiality, integrity, or availability could be expected to have a severe or catastrophic adverse effect on organizational operations, organizational assets, or individuals.  
- A severe or catastrophic adverse effect means that, for example, the loss of confidentiality, integrity, or availability might 
    - (i) cause a severe degradation in or loss of mission capability to an extent and duration that the organization is not able to perform one or more of its primary functions; 
    - (ii) result in major damage to organizational assets; 
    - (iii) result in major financial loss; or 
    - (iv) result in severe or catastrophic harm to individuals involving loss of life or serious life threatening injuries.  

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    {% set infos=ssp.information_types %}
    .. list-table:: Information Types 
        :name: Information Types

        * - Information Type
          - NIST 800-60 identifier
          - Confidentiality
          - Integrity
          - Availability
    {% for info in infos %}
        * - {{info.type}}
          - {{info.id}}
          - {{info.cia[0]}}
          - {{info.cia[1]}}
          - {{info.cia[2]}}
    {% endfor %}

Security Objectives Categorization (FIPS 199)
---------------------------------------------

Based on the information provided in :numref:`Information Types`. Sensitivity Categorization of Information Types, for the Enter Information System Abbreviation, default to the high-water mark for the Information Types as identified in :numref:`Security Impact Level` Security Impact Level below.  

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    .. list-table:: Security Impact Level
        :name: Security Impact Level

        * - Security Objective
          - Low Moderate or High
        * - Confidentiality
          - {{ssp.impact_level[0]}}
        * - Integrity
          - {{ssp.impact_level[1]}}
        * - Availability
          - {{ssp.impact_level[2]}}


Through review and analysis, it has been determined that the baseline security categorization for the Enter Information System Abbreviation system is listed in the :numref:`Baseline Security Configuration`. Baseline Security Configuration that follows. 

.. jinja:: builtins

    {% set ssp=builtins['ssp'] %}
    .. list-table:: Baseline Security Configuration
        :name: Baseline Security Configuration

        * - Security Objective
          - Low Moderate or High
        * - Confidentiality
          - {{ssp.impact_level[0]}}
        * - Integrity
          - {{ssp.impact_level[1]}}
        * - Availability
          - {{ssp.impact_level[2]}}

Using this categorization, in conjunction with the risk assessment and any unique security requirements, we have established the security controls for this system, as detailed in this SSP.  

Digital Identity Determination
------------------------------

The digital identity information may be found in Attachment 3, Digital Identity Worksheet.

The digital identity level is UNKNOWN