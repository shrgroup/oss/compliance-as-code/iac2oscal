import re, os

p = re.compile("\s{3}Sections/[0-9]{2}_(.*)")
# output_dir = ''
# if not os.path.isdir(output_dir):
#     os.makedirs(output_dir)

with open('index.rst') as f:
    files = [
        ( 
            p.search(l).group(0).strip(),
            p.search(l).group(1).strip(),
        )
        for l in f.readlines() 
        if p.search(l)
    ]
print(files)
for file in files:
    title = file[1].replace('_',' ').title()
    filename = "{}.rst".format(file[0])
    if os.path.isfile(filename):
        print("{} Exists, skipping".format(file[0]))
        continue
    with open(filename, 'w') as f:
        print("Creating {}".format(filename))
        f.write("{}\n{}\n".format(
                title, '='*len(title)
            )
        )
        
