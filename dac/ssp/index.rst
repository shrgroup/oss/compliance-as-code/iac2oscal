.. Example SSP documentation master file, created by
   sphinx-quickstart on Wed Jun 16 09:27:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Example SSP's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Sections/01_System_Name
   Sections/02_System_Categorization
   Sections/03_System_Owner
   Sections/04_Authorizing_Officials
   Sections/05_Other_Contacts
   Sections/06_Assignment_Responsibility
   Sections/07_System_Operational_Status
   Sections/08_Information_System_Type
   Sections/09_System_Description
   Sections/10_System_Environment_And_Inventory
   Sections/11_System_Interconnections
   Sections/12_Laws_Regulations_Standards
   Sections/13_Security_Controls
   Sections/14_Acronyms
   Sections/15_Attachments
