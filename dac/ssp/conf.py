project = 'Example SSP'
copyright = '2021, Matthew Ruge'
author = 'Matthew Ruge'

extensions = [
    'sphinx-oscal',
    'sphinxcontrib.jinja',
    'sphinx-fedramp'
]

oscal_sources = [
    {'source': 'local',
     'subpath': '../../iac/generated/ami_cdef.xml'},
    {'source': 'local',  # local doesn't "do" anything, anything that isn't git is processed as local
     'subpath': '../../iac/generated/ssp.xml'},
    {'source': 'https://github.com/usnistgov/oscal-content.git',
     'subpath': 'nist.gov/SP800-53/rev5/xml/NIST_SP-800-53_rev5_catalog.xml'},
    {'source': 'https://github.com/usnistgov/oscal-content.git',
     'subpath': 'nist.gov/SP800-53/rev5/xml/NIST_SP-800-53_rev5_HIGH-baseline_profile.xml'}

]

# Necessary for Sphinx-Oscal to populate environment with content
jinja_contexts = {
    'builtins': {'builtins': __builtins__}
}
numfig = True